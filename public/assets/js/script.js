/*
This code snippet was done with knockout, creation of the header.
*/
function headerNav() {
	// Template header
	var header = (
		'<header class="header">'+
			'<div class="top-links-area">'+
				'<div class="top-links-inside">'+
					'<div class="account">'+
						'<a href="#">Acesse sua Conta</a> <span>ou</span> <a href="#">Cadastre-se</a>'+
					'</div>'+
				'</div>'+
			'</div>'+
			'<div class="area-logo-search">'+
				'<div class="area-inside">'+
					'<!-- menu-mobile -->'+
					'<div class="main-menu">'+
						'<div class="menu-mob toggleMenu" id="menu-mob"></div>'+
						'<nav class="nav-menu-mob" id="top-menu">'+
							'<div class="content">'+
								'<div class="close" id="close"><span>X</span></div>'+
								'<ul class="navigation-menu nav-ul">'+
								'</ul>'+
							'</div>'+
						'</nav>'+
					'</div>'+
	
					'<div class="area-logo">'+
						'<a href="/index.html"><img src="media/logo-brand.png" alt="logo-marca do site" width="150" height="50"></a>'+
					'</div>'+
	
					'<div class="area-search">'+
						'<form class="form-search">'+
							'<div class="search" id="focused">'+
								'<input type="text" class="input-text" id="input">'+
								'<button class="button primary search">'+
									'<span class="text-button">Buscar</span>'+
								'</button>'+
							'</div>'+
	
							'<div class="mob-search">'+
								'<span></span>'+
							'</div>'+
						'</form>'+
					'</div>'+
				'</div>'+
			'</div>'+
			'</div>'+
				'<div class="bottom-black" id="bottom-black"></div>'+
			'</div>'+
		'</header>'+
	
		'<!-- nav -->'+
		'<nav class="navigation-area">'+
			'<div class="navigation-inside">'+
				'<ul id="mmenu" class="navigation-menu">'+
				'</ul>'+
			'</div>'+
		'</nav>'
	);
	
	// Variable to observe as the template header and sidebar changes
	var viewModel = {
		header: ko.observable(header)
	};
	
	// Run viewModel variable
	ko.applyBindings(viewModel);

}

// Consult the list of APIs and fill in the menu and the list in the sidebar
function renderMenu() {
	$.ajax({
		url : "/api/V1/categories/list",
		type: "GET",
		dataType: "json"
	}).done(function(data){
		$('.navigation-menu').append('<li class="nav-list"><a href="/index.html">Página inicial</a></li>');
		const links = data.items;
		$.each(links, function(menu_item) {
			$('.navigation-menu').append('<li class="nav-list"><a href="/category.html?cat=' + links[menu_item].id + '">' + links[menu_item].name + '</a></li>');
		});
		$('.navigation-menu').append('<li class="nav-list"><a href="/contact.html">Contato</a></li>');
	}).fail(function(){
		console.log("Erro na requisição");
	});
}

// Show mobile menu or hide
function toggleMenuMob() {
	$('.toggleMenu').click(function () {
		$('#top-menu').slideToggle('slow');
	});

	$('#close').click(function () {
		$('#top-menu').css('display', 'none');
	})

	$('.btn-buy').on('click', function () {
		$(this).attr
	});
}

// The array of colors
var filters = [];

// Defines the path to load items
function getPath() {
	// Checks the category and filters selected by parameter
	let queryString = window.location.search;
	let urlParams = new URLSearchParams(queryString);
	let cat = urlParams.get('cat');
	let category = cat === null ? loadAllProducts() : listByCategory(cat);
	return category;
}

// If there is no path defined it loads all categories
function loadAllProducts() {
	$('#products').empty();

	$.ajax({
		url : "/api/V1/categories/list",
		type: "GET",
		dataType: "json"
	}).done(function(list_categories){
		let items = list_categories.items;
		let i = 0;
		$.each(items, function () {
			listByCategory(items[i].id);
			i++
		});
		$('#categorie').html('Todas as Categorias');
	}).fail(function(){
			console.log("Erro na requisição");
	});
}

// List the products in the catalog according to the path
function listByCategory(category) {
	$.ajax({
		url: "/api/V1/categories/" + category,
		type: "GET",
		dataType: "json"
	}).done(function (list_products) {
		let i = 0;
		let items = list_products.items;

		// Receives product attribute values
		$.each(items, function (list) {
			let id = items[i].id;
			let name = items[i].name;
			let image = items[i].image;
			let price = items[i].price;
			let specialPrice = items[i].specialPrice;
			let filter = items[i].filter[0].color;
			let gender = items[i].filter[0].gender;

			if (filter !== undefined && $.inArray(filter, filters) === -1) {
				filters.push(filter);
				$('#colors').append('<li class="' + filter + '"><a href="?filter=' + filter + '"><img width="48" height="24" onclick="applyColor(event)" apply="' + filter + '" src="media/filters/' + filter + '.png' + '"></a></li>');
			}

			// Performs the treatment of products with promotion
			let currentPrice = specialPrice ? specialPrice : price;
			currentPrice = currentPrice.toLocaleString('pt-br', {
				style: 'currency',
				currency: 'BRL'
			});
			price = price.toLocaleString('pt-br', {
				style: 'currency',
				currency: 'BRL'
			});
			if (specialPrice == undefined) {
				$('.old-price').hide();
			}

			/*
			Render the product blocks on the screen
			Checks active filters before rendering products
			*/
			let product = (
				'<div id="product-' + id + '" class="item">'+
					'<div class="img">'+
						'<img src="' + image + '" alt="' + name + '">'+
					'</div>'+
					'<div class="name">'+
						'<div class="title">' + name + '</div>'+
						'<div class="price-container">'+
							'<div class="old-price">' + price +
							'</div>'+
							'<div class="price">' + currentPrice +
							'</div>'+
						'</div>'+
						'<div class="buy">'+
							'<button type="button" onclick="addToCart(event)" id=" ' + id + ' " price="' + price + '" image="' + image + '" class="btn-buy" name="' + name + '">Comprar</button>'+
						'</div>'+
					'</div>'+
				'</div>'
			);

			$('#products').append(product);
			if (gender) {
				$('#product-' + id).attr('gender', gender);
			}
			if (filter) {
				$('#product-' + id).attr('color', filter);
			}

			i++
		});
	}).fail(function () {
		console.log("Erro na requisição");
	});
}

// Displays the addition mode in the cart, simulating a purchase
function addToCart() {
	// Creates the popup and data for the selected product
	var name = event.target.attributes.name.value;
	var image = event.target.attributes.image.value;
	var price = event.target.attributes.price.value;

	$('#namePopup').html(name);
	$('#imgPopup').prop('src', image);
	$('#pricePopup').html(price);

	$('.addToCart, .windowCart, .addCartWindow').toggleClass('active');
	$('.addToCart, .windowCart, .addCartWindow').on('click', function () {
		$('.addToCart, .windowCart, .addCartWindow').removeClass('active');
	});
}

/*
Receives the desired filter by the click event, once the filter has been defined
performs the removal of items other than the selection.
*/
function applyColor() {
	event.preventDefault();
	let color = event.target.attributes.apply.value;
	$('.item').show();
	$('.item').filter("[color!='" + color + "']").hide();
}

function showFilter(f) {
	$('.categories').hide();
	$('.filter-' + f).toggle();
}

function updateBreadcrumbs() {
	// Update the category name in breadcrumbs
	let queryString = window.location.search;
	let urlParams = new URLSearchParams(queryString);
	let cat = urlParams.get('cat');

	if (cat === null) {
		$('#categorie').html('Todas as Categorias');
	} else {
		$.ajax({
			url : "/api/V1/categories/list",
			type: "GET",
			dataType: "json"
		}).done(function(catTitle){
			cat = cat - 1;
			$('#categorie').html(catTitle.items[cat].name);
			$('#categorieTitle').html(catTitle.items[cat].name);
		}).fail(function(){
			console.log("Erro na requisição");
		});
	}
}

function gridList() {
	let listing = event.target.attributes.id.value;

	if (listing === 'list') {
		$('#products, .item').addClass('list');
		$('#list').addClass('active');
		$('#grid').removeClass('active');

	} else if (listing === 'grid') {
		$('#products, .item').removeClass('list');
		$('#list').removeClass('active');
		$('#grid').addClass('active');
	}
}

// Render the page
function renderPage() {
	// Calls the function that defines the path to load elements according to selected items
	getPath();
	updateBreadcrumbs();
}

// Ready event, executes the functions. This event happens whenever the page is completely loaded
$(function () {
	renderPage();
	headerNav();
	renderMenu();
	renderPage();
	toggleMenuMob()
});
