module.exports = function (grunt) {
    require('jit-grunt')(grunt);

    grunt.initConfig({

        less: {
            development: {
                options: {
                    compress: true,
                    yuicompress: true,
                    optimization: 2
                },
                files: {
                    "public/assets/css/style.css": "public/assets/css/_extend.less"
                }
            }
        },

        watch: {
            stylesheets: {
                files: ["public/assets/css/**/*.less"],
                task: ["less"],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.registerTask("default", ["less", "watch"]);
    grunt.registerTask('dist', ['concat:dist', 'uglify:dist']);
};